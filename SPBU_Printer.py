# testing firmware for smart SPBU Printer

import time
import RPi.GPIO as GPIO
from pad4pi import rpi_gpio
import socket
from RPLCD.gpio import CharLCD
from escpos.connections import getUSBPrinter
import cups

state = 0
buff_req = ["R",":","0",":","0",":","E"]
lcd = CharLCD(pin_rs=16,pin_e=12,pins_data=[26,19,13,6],numbering_mode=GPIO.BCM,cols=16,rows=2,dotsize=8,charmap='A02',auto_linebreaks=True)
lcd.clear()

def printKey(key):
    global flag,state,disp_nmr,nzl_nmr,data_req
    flag = 0
    print(key)
    key2 = str(key)
    
    if flag + state == 0 :
        if key2 == "*":
            lcd.write_string("dispenser:")
            print (flag)
            state = 1
        else:
            state = 0
            lcd.write_string ("press * button")
            time.sleep(1)
            lcd.clear()
        
    elif flag + state == 1 :
        lcd.cursor_pos=(0,11)
        lcd.write_string(key2)
        disp_nmr = key2
        buff_req[2] = key2 #update buff req
        time.sleep(1)
        lcd.clear()
        lcd.write_string("Nozzle:")
        state = 2
    
    elif flag + state == 2:
        lcd.cursor_pos=(0,8)
        lcd.write_string(key2)
        nzl_nmr = key2
        buff_req[4] = key2 #update buff req
        data_req = ''.join(buff_req) #convert to string
        print (data_req)
        time.sleep(1)
        state = 0 #clear state
        lcd.clear()
        lcd.write_string("Printing ....")
        get_lan_data()
        write_file()
        conn.printFile (printer_name, file_printer, "Project Report", {}) # printing the file
        time.sleep(1)
        lcd.clear()

        
    #elif flag + state == 3:
    #    state = 0 #clear state
#   #     conn.printFile (printer_name, file, "Project Report", {}) # printing
    #    time.sleep(1)
    #    lcd.clear()

    
def get_lan_data ():
    global s, data_lan
    TCP_IP = "169.254.41.182"
    TCP_Port = 2019
    TCP_Buff = 30
    MESSAGE = data_req

    s = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
    s.connect ((TCP_IP,TCP_Port))
    s.send (MESSAGE)
    
    data_lan = s.recv(TCP_Buff)
    s.close()
    print ("recv data", data_lan)
    
def write_file ():
    file = open ("/home/pi/text.txt","w+")
    file.write ("SPBU Printer \r\n")
    file.write ("SPBU Number : 0812345 \r\n")
    file.write ("Dispenser : ")
    file.write (disp_nmr)
    file.write ("\r\n")
    file.write ("Nozzle: ")
    file.write (nzl_nmr)
    file.write ("\r\n")
    spbu_lan = list(data_lan.split(":")) # now data_lan splitted into list with delimiter :
    
    file.write ("Date : ")
    file.write (spbu_lan[3]) # print date at data lan
    file.write("\r\n")
    
    file.write ("Amount : ")
    file.write (spbu_lan[4]) # amount
    file.write("\r\n")
    
    file.close()
    file = open ("/home/pi/text.txt","r")
    read_char = file.read()
    print (read_char)

def keypad_init ():
    global keypad
    KEYPAD = [[1,2,3,"A"],[4,5,6,"B"],[7,8,9,"C"],["*",0,"#","D"]]
    row_pins = [17,11,3,2]#[9,10,22,27]         #BCM Mode
    col_pins = [9,10,22,27]#[17,11,3,2]           #BCM Mode
    factory = rpi_gpio.KeypadFactory()
    keypad = factory.create_keypad(keypad=KEYPAD,row_pins=row_pins,col_pins=col_pins)
    keypad.registerKeyPressHandler(printKey)

def lcd_init():
    global lcd
    lcd = CharLCD(pin_rs=16,pin_e=12,pins_data=[26,19,13,6],numbering_mode=GPIO.BCM,cols=16,rows=2,dotsize=8,charmap='A02',auto_linebreaks=True)
    
    
def tcpip_client_init():
    global TCP_IP,TCP_PORT,TCP_Buff,MESSAGE,s
    TCP_IP = "169.254.41.182"
    TCP_Port = 2019
    TCP_Buff = 30
    MESSAGE = "R:1:2:E"
    s = socket.socket (socket.AF_INET, socket.SOCK_STREAM) # init tcp ip cclient

def printer_init ():
    global conn, file_printer, printer_name
    conn = cups.Connection()
    printers = conn.getPrinters()
    file_printer = "/home/pi/text.txt"
    printer_name=printers.keys()[0]
    
def main ():
    keypad_init()
    tcpip_client_init()
    printer_init()
    try:
        while(True):
            time.sleep(0.1)
    except:
        keypad.cleanup()


if __name__ == "__main__":
    main()



















