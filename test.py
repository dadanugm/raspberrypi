# testing
import math
import numpy as np
import RPi.GPIO as GPIO
from RPLCD.gpio import CharLCD
import spidev
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(38,GPIO.OUT)
GPIO.setup(40,GPIO.OUT)
#init LCD Pin
lcd = CharLCD(pin_rs=36,pin_e=32,pins_data=[37,35,33,31],numbering_mode=GPIO.BOARD,cols=16,rows=2,dotsize=8,charmap='A02',auto_linebreaks=True)


print ("hello world")

def testing_gpio ():
    GPIO.output(38,GPIO.HIGH)
    GPIO.output(40,GPIO.HIGH)
    time.sleep(1)
    GPIO.output(38,GPIO.LOW)
    GPIO.output(40,GPIO.LOW)
    time.sleep(1)
    
def testing_lcd ():
    lcd.write_string('hello world')
    time.sleep(5)
    lcd.clear()
    
def main():
    while True:
#        testing_gpio()
        testing_lcd()
        
if __name__ == "__main__":
    main()















