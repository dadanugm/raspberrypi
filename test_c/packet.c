
#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <stdlib.h>
#include "tmp_val.h"

//tmp_v tmp;
//char temp_val;
FILE *fptr;

void my_callback(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* packet);

int main(int argc, char *argv[])
{
//	FILE *fptr;
	printf("hello world\n");
	
	pcap_t *handle;			/* Session handle */
	char *dev;			/* The device to sniff on */
	char errbuf[PCAP_ERRBUF_SIZE];	/* Error string */
	struct bpf_program fp;		/* The compiled filter */
	char filter_exp[] = "port 1883";	/* The filter expression */
	bpf_u_int32 mask;		/* Our netmask */
	bpf_u_int32 net;		/* Our IP */
	struct pcap_pkthdr header;	/* The header that pcap gives us */
	const u_char *packet;		/* The actual packet */

	/* Define the device */
	dev = pcap_lookupdev(errbuf);
	if (dev == NULL) {
		fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
		return(2);
	}
	/* Find the properties for the device */
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
		fprintf(stderr, "Couldn't get netmask for device %s: %s\n", dev, errbuf);
		net = 0;
		mask = 0;
	}
	/* Open the session in promiscuous mode */
	handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		return(2);
	}
	/* Compile and apply the filter */
	if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
		fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
		return(2);
	}
	if (pcap_setfilter(handle, &fp) == -1) {
		fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
		return(2);
	}
	/* Grab a packet */
	packet = pcap_next(handle, &header);
	/* Print its length */
	printf("Jacked a packet with length of [%d]\n", header.len);
	pcap_loop(handle, -1, my_callback, NULL); 
	/* And close the session */
	pcap_close(handle);
	return(0);
}


void my_callback(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char*  packet) 
{ 
    int i=0; 
    static int count=0; 
  
//    printf("Packet Count: %d\n", ++count);    /* Number of Packets */
//    printf("Recieved Packet Size: %d\n", pkthdr->len);    /* Length of header */
//    printf("Payload:\n");                     /* And now the data */
    for(i=0;i<pkthdr->len;i++) 
	{ 
//        if(isprint(packet[i]))                /* Check if the packet data is printable */
//            printf("%c ",packet[i]);          /* Print it */
//        else
//            printf("",packet[i]);          /* If not print a . */
	
	if (packet[i]=='s')
	{
		i++;
		if (packet[i]=='t')
    		{
			i++;
			if (packet[i]=='a')
			{
				printf ("%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", packet[i-3],packet[i-2],packet[i-1],packet[i],packet[i+1],packet[i+2],packet[i+3],packet[i+4],packet[i+5],packet[i+6],packet[i+7],packet[i+8],packet[i+9],packet[i+10]);
				// change tmp_value in tmp_val.h
//				tmp.tmp_value = packet[i+10];
//				printf ("%c",tmp.tmp_value);
				temp_val = packet[i+10];
				printf ("%c",temp_val);
//				//save value to file
				fptr = fopen("temp_file","w");
				fprintf(fptr,"%c",temp_val);
				fclose(fptr);
			}
    		} 
	}
        
	if((i%50==0 && i!=0) || i==pkthdr->len-1){ 
            printf("\n"); 
    }
//	printf ("%c",tmp.tmp_value); 
}
}
