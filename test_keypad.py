#testing keypad

import RPi.GPIO as GPIO
from RPLCD.gpio import CharLCD
from pad4pi import rpi_gpio
import time

lcd = CharLCD(pin_rs=16,pin_e=12,pins_data=[26,19,13,6],numbering_mode=GPIO.BCM,cols=16,rows=2,dotsize=8,charmap='A02',auto_linebreaks=True)
keypad = [[1,2,3,"A"],[4,5,6,"B"],[7,8,9,"C"],["*",0,"#","D"]]

row_pins = [17,11,3,2]#[9,10,22,27]         #BCM Mode
col_pins = [9,10,22,27]#[17,11,3,2]           #BCM Mode

factory = rpi_gpio.KeypadFactory()
keypad = factory.create_keypad(keypad=keypad,row_pins=row_pins,col_pins=col_pins)

def printKey(key):
    print(key)
    key2 = str(key)
    lcd.write_string(key2)

keypad.registerKeyPressHandler(printKey)

try:
  while(True):
    time.sleep(0.2)
except:
 keypad.cleanup()








