# testing SPI raspi

import spidev
import time

spi = spidev.SpiDev()
spi.open(0,1)
spi.max_speed_hz = 1000
spi.mode = 0b01

try:
    while True:
        resp = spi.xfer2([0xAA])
        time.sleep(0.1)
        
except KeyboardInterrupt :
    spi.close()