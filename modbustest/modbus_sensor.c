#include <stdio.h>
#include <errno.h>
#include <inttypes.h>
#include <modbus.h>

// slave number
#define tmp_sensor 1

int tmp_addr = 0;
int tmp_ln = 1;
uint16_t buffer[16];

void read_sensor(void);

int main()
{
	
	modbus_t *ctx;
	
	// connect modbus
	ctx = modbus_new_tcp("127.0.0.1", 1502);
    modbus_set_debug(ctx, TRUE);
	modbus_set_slave(ctx,tmp_sensor);
	
	while (1)
	{
		read_sensor ();
	}
}

/*
	This function used to read all available modbus based sensor
	modify when needed.
*/
void read_sensor(void)
{
	modbus_t *ctx;
	// command to read holding register example, temperature sensor
	modbus_read_registers(ctx,tmp_addr,tmp_ln,buffer);
}



// list function on modbus.h
/*
	int modbus_read_registers(modbus_t *ctx, int addr, int nb, uint16_t *dest) // read holding registers (func code :03)
	int modbus_read_input_registers(modbus_t *ctx, int addr, int nb, uint16_t *dest) // read input register (func code: 04)
	int modbus_read_bits(modbus_t *ctx, int addr, int nb, uint8_t *dest) // Reads the boolean status of bits and sets the array elements in the destination to TRUE or FALSE (single bits)
	int modbus_read_input_bits(modbus_t *ctx, int addr, int nb, uint8_t *dest) //Same as modbus_read_bits but reads the remote device input table 
	static int write_single(modbus_t *ctx, int function, int addr, int value) //Write a value to the specified register of the remote device. Used by write_bit and write_register
	int modbus_write_bit(modbus_t *ctx, int addr, int status) //Turns ON or OFF a single bit of the remote device
	int modbus_write_bits(modbus_t *ctx, int addr, int nb, const uint8_t *src) // Write the bits of the array in the remote device
	int modbus_write_registers(modbus_t *ctx, int addr, int nb, const uint16_t *src) //	 Write the values from the array to the registers of the remote device
	
	int modbus_write_and_read_registers(modbus_t *ctx,
                                    int write_addr, int write_nb,
                                    const uint16_t *src,
                                    int read_addr, int read_nb,
                                    uint16_t *dest)
									
									// Write multiple registers from src array to remote device and read multiple
   registers from remote device to dest array.
   
   int modbus_set_slave(modbus_t *ctx, int slave) //   * Define the slave number 
   
   
	
*/