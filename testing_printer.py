# testing package for printer

from escpos.connections import getNetworkPrinter
from escpos.connections import getUSBPrinter
from escpos.connections import getFilePrinter
from escpos.connections import getSerialPrinter

def ip_printer():
    global printer_IP
    printer_IP = getNetworkPrinter()(host='192.168.128.100')


def usb_printer():
    global printer_USB
    printer_USB = getUSBPrinter()(idVendor=0x04b8,idProduct=0x08d1,interface = 0x06,inputEndPoint=0x83,outputEndPoint=0x04)
    
def file_printer():
    global printer_FILE
    printer_FILE = getFilePrinter(commandSet='Generic')(dev='/ttys0')
    
def serial_printer():
    global printer_SER
    printer_SER = getSerialPrinter()(dev='/dev/ttyS0',baudrate=16550,bytesize=8,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,timeout=1.00,dsrdtr=True)

#ip_printer()    
#printer_IP.text('hello world')
#printer_IP.lf()

usb_printer()
printer_USB.text('hello world')
printer_USB.lf()

#file_printer()
#printer_FILE.text('hello world')
#printer_FILE.lf()

#serial_printer()
#printer_SER.text('hello world')
#printer_SER.lf()




    
