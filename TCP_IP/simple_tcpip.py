#simple of tcp-ip with python

import socket

TCP_IP = '169.254.72.34'
TCP_PORT = 2002
TCP_BUFFER = 20

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind ((TCP_IP,TCP_PORT))
s.listen(1)

conn,addr = s.accept()
print 'connection address', addr
while 1:
    data=conn.recv(TCP_BUFFER)
    if not data: break
    print 'received data',data
    conn.send(data)
conn.close()

