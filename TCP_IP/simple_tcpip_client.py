# tcp ip simple client

import socket

TCP_IP = "169.254.41.182"
TCP_Port = 2019
TCP_Buff = 30
MESSAGE = "R:1:2:E"

s = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
s.connect ((TCP_IP,TCP_Port))
s.send (MESSAGE)

data = s.recv(TCP_Buff)
s.close()
print ("recv data", data)