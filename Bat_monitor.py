# this code is using RPi I2C to monitoring battery condition

from smbus2 import SMBus, i2c_msg
import time

#list address
dev_addr = 0x55
Wr_addr = 0xAA
Rd_addr = 0xAB

#list register (cmd) Read only memory
ctrl_cmd = 0x00 #control status
mac_data = 0x40 #
temp_cmd = 0x06 #temperature
volt_cmd = 0x08 #voltage
batt_cmd = 0x0A # battery status
curr_cmd = 0x0C #current
desc_cmd = 0x3C #design capacity
temp_int = 0x28 #internal temperature
rem_capc = 0x10 #remaining capacity
fulc_cap = 0x12 #full charge capacity
avr_curr = 0x14 #Average current
time_emp = 0x16 #time to empty
time_ful = 0x18 #time to full
acc_chrg = 0x1A #accumulated charge
acc_time = 0x1C #accumulated time
last_acc = 0x1E #last accumulated charge
last_act = 0x20 #last accumulated time
avrg_pwr = 0x24 #average power
cycl_cnt = 0x2A # cycle count
relv_stt = 0x2C #relative state of charge
stat_hlt = 0x2E #state of health
chrg_vlt = 0x30 #charging voltage
chrg_crr = 0x32 #charging current
BLT_disc = 0x34 #BLT Discharge set
BLT_chag = 0x36 #BLT Discharge set
opt_stat = 0x3A #operation status


manu_acc = 0x3E #manufacturing access control
macd_sum = 0x60 # MAC data sum addr
macd_len = 0x61 #MAC data length

# control status sub command
con_stat = [0x00,0x00] #little endian
dev_type = [0x01,0x00]
fw_version = [0x02,0x00]
hw_version = [0x03,0x00]

#flash address
# Based on class
# class Calibration
cc_gain = [0x00,0x40] #little endian
cc_delta = [0x04,0x40]

#class Gas gauging
design_capacity = [0xF5,0x41]


#####################################
# define class and object
i2c = SMBus(1)
i2c_m = i2c_msg
#####################################
#example read data from read only register
#data = i2c.read_word_data(int(dev_addr),int(register))
# example read control status
#i2c.write_i2c_block_data(int(dev_addr),ctrl_cmd,hw_version)
#time.sleep(0.1)
#get_data = i2c.read_word_data(int(dev_addr),mac_data)

#testing read data from the flash

#read write data using i2c_msg
#wr = i2c_m.write(int(dev_addr),[read_temp])
#rd = i2c_m.read(int(dev_addr),2)
#read_data2 = i2c.i2c_rdwr(wr,rd)
#################### function ###################################
# this function used to read Read only register
def i2c_req_data (register):
    data = i2c.read_word_data(int(dev_addr),int(register))
    print (hex(data))

# this function to read status from Control status register (0x00)
def i2c_req_status (subctrl_cmd):
    i2c.write_i2c_block_data(int(dev_addr),int(ctrl_cmd),subctrl_cmd)
    time.sleep(0.1)
    get_data = i2c.read_word_data(int(dev_addr),int(mac_data))
    print (hex(get_data))

##this function is used to rewrite data flash on read-write memory
#def i2c_flash_rewrite (reg_addr, data):
#    ## write to manufacture
#    i2c.write_i2c_block_data(int(dev_addr),int(manu_acc),design_capacity)
#    # write data to mac data (0x40, through 0x5F)
#    i2c.write_byte_data(int(dev_addr),int(mac_data),int(0x50))
#    # Write 0xAB (complement of the sum of the ManufacturerAccessControl() and MACData() bytes) to MACDataSum(0x60).
#    i2c.write_byte_data(int(dev_addr),int(macd_sum),int(0x71)
#    # Write 0x08 (4 + length of MACData() bytes) to MACDataLen(0x61)
#    i2c.write_byte_data(int(dev_addr),int(macd_len),int(0x05))

## this function is used to calculate one complements of data
#def one_comp_calc (*args):
#    i = []
#    for hex_val in args:
#        i.append(hex_val)
#        j = sum(i)
#    print(hex(j))
#    print(hex(j ^ 255))



################## test ######################################
#print data
i2c_req_data(temp_int)
i2c_req_data(batt_cmd)
i2c_req_data(volt_cmd)
i2c_req_data(curr_cmd)
i2c_req_status(fw_version)
i2c_req_status(hw_version)
i2c_req_data(rem_capc)
i2c_req_data(fulc_cap)
#one_comp_calc(0x11,0x12,0x13)


#def main():
#    while(True):
#        send_data = i2c.write_byte(0x55,0x06)
#        sleep(1)
#
#if __name__ == "__main__":
#    main()
































